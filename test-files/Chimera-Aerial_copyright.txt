Digital Video Sequence "Chimera-Aerial"
Copyright (C)  NETFLIX INC.  2015 

480x264p, 10-bit, YCbCr 4:2:0, BT.709, 29.97 fps, 150 frames

COPYRIGHT AND LICENSE INFORMATION
December, 2015

NETFLIX INC.
100 Winchester Circle, Los Gatos, CA 95032, USA

The video sequences provided above and all intellectual property rights therein 
remain the property of Netflix Inc. This video sequence is licensed under the 
Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International 
License. To view a copy of this license, visit 
http://creativecommons.org/licenses/by-nc-nd/4.0/
