import os
import json
import hashlib
import multiprocessing
import shutil
import subprocess
import tarfile
import time
import tqdm


def main():
    target_folders = [r'C:\\']
    # list of folders to search for duplicates in
    compression_flag = False
    # if true, all bitstream folders will be compressed and the originals deleted
    use_zstd = True
    # if true and zstd.exe is in the same directory as the script, zstd will be used for compression 
    # (faster but potentially worse compression)


    storage_directory = r'C:\\'

    combined_folder_names = "_".join(
        [folder.rsplit(os.sep, maxsplit=1)[-1] for folder in target_folders])
    back_up_path = os.path.join(storage_directory, combined_folder_names)
    info_file_log = os.path.join(
        back_up_path, '{}_file_info.txt'.format(combined_folder_names))
    bitstream_log = os.path.join(
        back_up_path, '{}_bitstreams.txt'.format(combined_folder_names))
    duplicates_log = os.path.join(
        back_up_path, '{}_duplicates.txt'.format(combined_folder_names))

    try:
        os.makedirs(back_up_path)
    except FileExistsError:
        pass

    print('Info file: {}'.format(info_file_log))
    all_files = list()
    bitstream_folders = list()
    unhashed_files = list()
    file_info = dict()
    if os.path.isfile(info_file_log):
        print('Using previously found file hashes...')
        with open(info_file_log) as file:
            file_info_data = file.read()
        file_info = json.loads(file_info_data)
        print('There are {} files already hashed'.format(len(file_info)))

    print("Starting new search for duplicate files in {}".format(target_folders))
    start_time = time.time()

    for target_folder in target_folders:
        new_files, new_bitstream_folders = get_all_files(target_folder)
        all_files.extend(new_files)
        bitstream_folders.extend(new_bitstream_folders)

    print("Bitstream folders: {}".format(bitstream_folders))
    print('File Scan: Time taken {} seconds'.format(time.time()-start_time))
    print('There are {} files in {}'.format(
        len(all_files), combined_folder_names))

    with open(bitstream_log, 'w') as file:
        file.write('\n'.join(bitstream_folders))

    if compression_flag:
        start_time = time.time()
        if os.path.exists("{}zstd.exe".format(os.curdir + os.sep)) and use_zstd:
            print("Using zstd for compression")
            compress_bitstream_folders_zstd(bitstream_folders)
        else:
            print("zstd.exe not found, defaulting to tarfile compression")
            compress_bitstream_folders_tarfile(bitstream_folders)
        
        print('Bitstream Compression: Time taken {} seconds'.format(
            time.time()-start_time))

        print("Deleting all original uncompressed bitstream folders...{}".format(
            bitstream_folders))
        start_time = time.time()
        # delete_files(bitstream_folders)     #Don't uncomment until compression is fully tested
        print('Bitstream Deletion: Time taken {} seconds'.format(
            time.time()-start_time))

    start_time = time.time()
    unhashed_files = get_unhashed_files(all_files, file_info)

    print('There are {} files to be hashed. Time taken {} seconds'.format(
        len(unhashed_files), (time.time()-start_time)))

    if unhashed_files:
        print('Creating hashes for all unhashed files...')
        start_time = time.time()
        new_file_info = execute_parallel_hashing(unhashed_files)
        file_info.update(new_file_info)

        with open(info_file_log, 'w') as file:
            file.write(json.dumps(file_info, indent=1))
        end_time = time.time()
        total_time = end_time - start_time
        print('File Hash: Time taken {} seconds'.format(total_time))

    print('Length of file_info : {}'.format(len(file_info)))

    start_time = time.time()
    print('Checking if any duplicates were found...')
    duplicates = find_duplicates(file_info)

    print('Duplicate Search: Time taken {} seconds'.format(time.time()-start_time))

    print('Deleting duplicate files...')
    duplicates, files_to_delete = delete_duplicates(duplicates)
    # delete_files(files_to_delete)   #Uncomment this to actually delete the identified files

    with open(duplicates_log, 'w') as file:
        file.write(
            '["FileSize in GB", "Locations of all copies of the file"]\n')
        sorted_duplicates = sorted(
            list(duplicates.values()), key=lambda x: x[0], reverse=True)

        for sorted_duplicate in sorted_duplicates:
            file.write("\n")
            file_size_string = str(sorted_duplicate[0]) + ":\n\t"
            file.write(file_size_string)
            for file_path in sorted_duplicate[1:]:
                file.write('{}\n\t'.format(file_path))

        print('There are {} files with duplicates'.format(len(duplicates)))


def get_all_files(target_folder):
    """returns a list of all files found in the target directory"""
    all_files = list()
    bitstream_folders = list()
    count = 0
    for root, dirs, files in os.walk(target_folder):
        all_files.extend([os.path.join(root, file) for file in files])
        for directory in dirs:
            if directory == "bitstreams":
                folder_path = os.path.join(root, directory)
                print('Found matching directory {}'.format(folder_path))
                bitstream_folders.append(folder_path)
        if count % 100 == 0:
            print('{} files scanned'.format(len(all_files)))
        count += 1
    return all_files, bitstream_folders


def compress_bitstream_folders_tarfile(bitstream_folders):
    for bitstream_folder in bitstream_folders:
        root_directory = bitstream_folder.rsplit(os.sep, maxsplit=1)[0] + os.sep
        print("Root directory: {}".format(root_directory))
        print('Compressing {}'.format(bitstream_folder))
        with tarfile.open("{}compressed_bitstreams.tar.bz2".format(root_directory), "w:bz2") as tarhandle:
            count = 0
            for root, dirs, files in os.walk(bitstream_folder):
                for file in files:
                    tarhandle.add(os.path.join(root, file), arcname=file)
                    count += 1
                    print('{} files compressed'.format(count))

def compress_bitstream_folders_zstd(bitstream_folders):
    for bitstream_folder in bitstream_folders:
        root_directory = bitstream_folder.rsplit(
            os.sep, maxsplit=1)[0] + os.sep
        cmd = r"zstd -r -T0 --format=gzip {} -o {}compressed_bitstreams.gz".format(
            bitstream_folder, root_directory)
        print("Compressing Bitstream Folders: {}".format(bitstream_folder))
        pipe = subprocess.Popen(
            cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = pipe.communicate()
        pipe.wait()
        print("{}".format(output,error))
    return


def get_unhashed_files(all_files, file_info):
    """Filters out the files that have already been hashed in a previous run"""
    unhashed_files = list()
    count = 0
    for file in all_files:
        if file not in file_info:
            unhashed_files.append(file)
        count += 1
        if count % 100 == 0:
            print('Searching for previous hashes, {} files of {}'.format(
                count, len(all_files)))
    return unhashed_files


def execute_parallel_hashing(list_of_files):
    """Returns a list of lists ie [hash,filesize,filename] for all files using multiprocessing"""
    pooler = multiprocessing.Pool(
        processes=multiprocessing.cpu_count(), maxtasksperchild=30)
    hashed_files = list()

    chunk_size = len(list_of_files) // multiprocessing.cpu_count()
    if chunk_size == 0:
        chunk_size = 1
        # prevent error if list_of_files is too short
    for file in tqdm.tqdm(pooler.imap_unordered(compute_hash, list_of_files, chunk_size),
                          total=len(list_of_files), miniters=100):
        hashed_files.append(file)

    pooler.close()
    pooler.join()
    new_file_info = dict()

    for hashed_file in hashed_files:
        if hashed_file[0] is not None:
            file = hashed_file[0]
            new_file_info[file] = dict()
            new_file_info[file]['hash'] = hashed_file[1]
            new_file_info[file]['size'] = hashed_file[2]

    return new_file_info


def find_duplicates(file_info):
    """Takes the list of all file hashes and filters it to only those with duplicates"""
    count = 0
    duplicates = dict()

    for file in file_info:
        md5 = file_info[file]['hash']
        size = file_info[file]['size']
        if md5 not in duplicates:
            duplicates[md5] = [size, file]
        else:
            duplicates[md5].append(file)

        if count % 100 == 0:
            print('{} files sorted'.format(len(duplicates)))
        count += 1

    duplicate_count = 0
    for md5 in list(duplicates):
        if len(duplicates[md5]) == 2:
            del duplicates[md5]
        duplicate_count += 1
        if duplicate_count % 100 == 0:
            print('{} duplicates found'.format(duplicate_count))

    return duplicates


def delete_duplicates(duplicate_dict):
    """Deletes duplicate files according to a priority scheme"""
    whitelists = [r'C:\\']
    files_to_delete = list()
    for file_hash in duplicate_dict:
        duplicate_list = duplicate_dict[file_hash]
        if any(whitelist.strip().lower() in str(duplicate).strip().lower() for duplicate in duplicate_list[1:] for whitelist in whitelists):
            for index, duplicate in enumerate(duplicate_list[1:]):
                if not any(whitelist.strip().lower() in str(duplicate).strip().lower() for whitelist in whitelists):
                    # delete duplicates not in whitelist directory
                    files_to_delete.append(duplicate_list[index + 1])
                    duplicate_dict[file_hash][index +
                                              1] = duplicate_list[index + 1] + "_DELETED"
        else:
            # No files in whitelist, deleting duplicates by order of appearance
            for index, duplicate in enumerate(duplicate_list[2:]):
                # delete all files except for the first one
                files_to_delete.append(duplicate_list[index + 2])
                duplicate_dict[file_hash][index +
                                          2] = duplicate_list[index + 2] + "_DELETED"
    return duplicate_dict, files_to_delete


def delete_files(files_to_delete):
    for file_path in files_to_delete:
        if os.path.isfile(file_path):
            try:
                os.remove(file_path)
            except PermissionError:
                print(
                    'Failed to delete file {} due to permission error'.format(file_path))
        elif os.path.isdir(file_path):
            shutil.rmtree(file_path, ignore_errors=False)


def compute_hash(file_name):
    """Computes the hash and size for the given file"""
    hash_md5 = hashlib.md5()
    try:
        stat_info = os.stat(file_name)
        file_size = stat_info.st_size
        file_size = round(float(file_size/(1024*1024*1024)), 6)
    except (FileNotFoundError, PermissionError):
        return [None, None, None]
    try:
        with open(file_name, "rb") as file:
            for chunk in iter(lambda: file.read(65536), b""):
                hash_md5.update(chunk)
    except (FileNotFoundError, PermissionError):
        print("An exception occurred while creating the hash for ", file_name)
        return [None, None, None]
    return [file_name, hash_md5.hexdigest(), file_size]


if __name__ == '__main__':
    main()
