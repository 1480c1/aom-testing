import subprocess
import os
import multiprocessing
import sys 
import time
import glob

number_of_cores = multiprocessing.cpu_count()

command_to_test = '''(/usr/bin/time --verbose ./SvtAv1EncApp -i /home/inteladmin/psao/Issue2016_2022-12-05/football.y4m --rc 1 --tbr 2310 --mbr 4620 --keyint 2s --passes 2 --lp 56 --preset {preset} -b football_p5_M{preset}.ivf ) > football_p5_M{preset}.txt 2>&1'''
metric_command  = '''(/usr/bin/time --verbose /home/inteladmin/psao/Issue2016_2022-12-05/ffmpeg -y -nostdin  -r 25 -i football_p5_M{preset}.ivf  -r 25  -i /home/inteladmin/psao/Issue2016_2022-12-05/football.y4m -lavfi '[0:v][1:v]libvmaf=aom_ctc=1:log_path=football_p5_M{preset}.xml:log_fmt=xml:n_threads=56' -f null - ) > football_p5_M{preset}.log 2>&1'''

presets_to_test = [5,6] #
search_method   = 0     # 0: By Commit | 1: By Tag


def main():
    encoder_dir = os.path.join(os.getcwd(),'SVT-AV1')
    running_processes = dict()
    evalutated_commit = 0

    '''clone_repo'''
    call_system(["git clone https://gitlab.com/AOMediaCodec/SVT-AV1.git", os.getcwd()])
    
    '''get list of all commits'''
    commits = get_commits(encoder_dir)[:100]#[100:]

    '''Build command with issue'''
    commands = get_commands()
    commit_folders = list()
    lower = 0
    upper = len(commits)-1

    '''Build initial quete to search in order to find the initial upper and lower bounds'''
    queue = build_queue(lower,upper)
    print('queue',queue)
    
    for index in range(upper+1):
        commit_folders.append(os.path.join(os.getcwd(),commits[index]))
    

    while (upper - lower) != 1:
        for index in queue:
            cpu_usage = get_cpu_usage()

            prev_upper = upper
            prev_lower = lower
            if evalutated_commit and evalutated_commit > upper:
                print('Killing Upper Jobs')
                kill_jobs(evalutated_commit, upper,running_processes,commits)
                continue
            if evalutated_commit and evalutated_commit < lower:
                print('killing Lower Jobs')
                kill_jobs(evalutated_commit, lower,running_processes,commits)
                continue
                
            '''Monitor CPU Usage'''
            while cpu_usage > 90:
                cpu_usage = get_cpu_usage()
                print("CPU Usage too high, delaying further pooling")
                print('cpu_usage',cpu_usage)
                time.sleep(10)
                
            '''Create commit folder'''
            commit = commits[index]
            commit_folder = commit_folders[index]
            if not os.path.isdir(commit_folder):
                os.mkdir(commit_folder) 
                
            '''Check to see if there are any results from the commit folders'''
            passed_commits, failed_commits = evaluate_commits(index,commits)

            '''Change bound depending on result'''
            for passed_commit in passed_commits:
                upper = min(commit_folders.index(passed_commit),upper)

            for failed_commit in failed_commits:
                lower = max(commit_folders.index(failed_commit),lower)

            if prev_upper != upper or prev_lower != lower:
                break
                
            '''Run the test on the commit'''
            prepare_test_environment(commit, encoder_dir, commit_folder)
            running_processes = run_tests(commit, encoder_dir, commands, commit_folder, running_processes)
            print('running_processes',running_processes)
        
        print('upper',upper)
        print('lower',lower)            
        '''Rebuild the queue using the new lower and upper'''    
        queue = build_queue(lower,upper)
        print('queue',queue)
        for passed_commit, failed_commit in zip(passed_commits, failed_commits):
            if commit_folders.index(passed_commit) in queue:
                queue.remove(commit_folders.index(passed_commit))
                
            if commit_folders.index(failed_commit) in queue:
                queue.remove(commit_folders.index(failed_commit))
            
            

        print('New Queue',queue)
    print('problem commit is: ', lower)
    return lower


def kill_jobs(evalutated_commit, bound,running_processes,commits):
    '''Eliminate uppoer range commits'''
    if evalutated_commit > bound:
        for job in range(evalutated_commit, bound):
            kill(job, running_processes, commits)
    else:
        for job in range(bound, evalutated_commit):
            kill(job, running_processes, commits)


def kill(jobid,running_processes):
    for commit_folder in running_processes:
         if commits[jobid] in commit_folder:
            for process in running_processes[commit_folder]:
                process.kill()
            running_processes[commit_folder] = list()


def evaluate_commits(index,commits):
    folders = glob.glob('%s/*'%os.getcwd())

    passed_commits = list()
    failed_commits = list()
    
    for folder in folders:
        vmaf_lows = list()
        vmaf_avgs = list()
    
        folder_files = glob.glob('{}/*.xml'.format(folder))
        for folder_file in folder_files:
            with open(folder_file) as xml_result:
                print('folder_file',folder_file)
                content = xml_result.read()
                preset = folder_file.split('_M')[-1].split('.')[0]
                
                metric_section = content.split('<metric name="vmaf" min="')[-1]
                vmaf_low = metric_section.split('"')[0]
                print('vmaf_low',vmaf_low)
                vmaf_avg = metric_section.split('" />')[0].split('"')[-1] 
                print('vmaf_avg',vmaf_avg)
                vmaf_lows.append(vmaf_low)
                vmaf_avgs.append(vmaf_avg)
                
        if len(vmaf_lows) == 2 and len(vmaf_avgs) == 2:
            if (vmaf_lows[0] > vmaf_lows[1]) or (vmaf_avgs[0] > vmaf_avgs[1]):
                passed_commits.append(folder)
            else:
                failed_commits.append(folder)
        print('passed_commits',passed_commits)
        print('failed_commits',failed_commits)
        # input()
    return passed_commits, failed_commits
    

def build_queue(lower,upper):
    queue = list()
    lower1 = lower2 = lower
    upper1 = upper2 = upper

    queue.append(lower1)
    queue.append(upper1)
    
    while (upper1 - lower1) > 1:
        mid1 = int((upper1 - lower1)/2 + lower1)
        mid2 = int((upper2 - lower2)/2 + lower2)
        
        if mid1 not in queue:
            queue.append(mid1)
        if mid2 not in queue:
            queue.append(mid2)
            
        lower1 = mid1
        upper2 = mid2
    
    return queue


def get_commits(encoder_dir):
    '''get desired commit'''
    commits, error = call_system(["git describe --tags `git rev-list --tags --max-count=700`",encoder_dir])
    commits = commits.splitlines()
    
    return commits


def get_commands():
    commands = list()
    
    for preset in presets_to_test:
        command = '{} && {}'.format(command_to_test.format(**vars()), metric_command.format(**vars()))
        commands.append(command)
        
    return commands
    

def prepare_test_environment(commit, encoder_dir, commit_folder):
    print('Preparing Commit: {}'.format(commit))
        
    call_system(['git checkout {}'.format(commit), encoder_dir])         
    call_system(['Build/linux/build.sh static release',encoder_dir])
    call_system(['cp {}/Bin/Release/* {}'.format(encoder_dir, commit_folder), os.getcwd()])


def run_tests(commit,encoder_dir,commands, commit_folder,running_processes):
    cpu_usage = get_cpu_usage()
    
    '''run_tests'''
    for command in commands:
        process = subprocess.Popen(command, shell=True, cwd=commit_folder)
        if commit_folder not in running_processes:
            running_processes[commit_folder] = [process]
        else:
            running_processes[commit_folder].append(process)
            
    return running_processes
    
    
def get_cpu_usage():
    output,error = call_system(["top -bn2 | grep '%Cpu' | tail -1 | grep -P  '(....|...) id,'", os.getcwd()])
    cpu_usage = float(output.split(' us')[0].split(' ')[-1])    
    return cpu_usage

    
def call_system(inputs):
    cmd, work_dir = inputs
    pipe = subprocess.Popen(cmd, shell=True, cwd=work_dir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = pipe.communicate()
    pipe.wait()
    
    return output,error
    
    
if __name__ == '__main__':
    main()
