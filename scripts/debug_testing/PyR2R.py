import os
import shutil
from multiprocessing import Pool
from filecmp import cmp
import subprocess

instances = 20
preset = 9
encoderpath = './'
clip = 'input.y4m'

special_parameters = '--passes 1 -q 60 --keyint -1'

def create_directories():
    try:
        shutil.rmtree('logs/')
        shutil.rmtree('bitstreams/')
    except:
        pass

    os.mkdir('logs')
    os.mkdir('bitstreams')

def run_subprocess(cmd):
    log_file = cmd[0]
    cmd = cmd[1:]
    print(cmd)
    with open(log_file, 'w') as log:
        subprocess.Popen(cmd, stdout=log, stderr=subprocess.STDOUT).wait()

def run_commands():
    if not os.path.isfile(clip):
        return
    cmds = []
    basefn = '_M{}.bin'.format(preset)
    app_path = os.path.join(encoderpath, "SvtAv1EncApp")
    for i in range(instances):
        cmd = [
            'logs/dump{}.txt'.format(i+1),
            app_path,
            "-i",
            clip,
            "--preset",
            str(preset),
        ]
        cmd.extend(special_parameters.split())
        cmd.extend(["-b",
                    "bitstreams/" + str(i+1) + basefn,
                    ])
        cmds.append(cmd)
        
    Pooler = Pool(processes=112)
    Pooler.map(run_subprocess, cmds)

    for i in range(instances - 1):
        if not cmp('bitstreams/' + str(i+1)+basefn,'bitstreams/' + str(i+2)+basefn):
            print 'R2R :< ' + str(i+1)+basefn + '\t' + str(i+2)+basefn

    raw_input('DONE!')
    
def main():
    create_directories()
    run_commands()

if __name__ == "__main__":
    main()

