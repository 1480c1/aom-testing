mkdir -p localinstalls
localinstalls=$PWD/localinstalls

# clone and set-up repo
git clone https://gitlab.com/aomediacodec/SVT-AV1.git svt
git -C svt checkout v1.4.0

build_svt() (
git -C svt apply < fix.patch
pref=$1
./svt/Build/linux/build.sh --prefix "$localinstalls/$pref" release install static
mv ./svt/Bin/Release "./svt/Bin/$pref"
git -C svt log -3 > "./svt/Bin/$pref/hash.txt"
git -C svt diff > "./svt/Bin/$pref/diff.patch"
git -C svt reset --hard HEAD
)

# build reference
git -C svt log > hash.txt
git -C svt reset --hard HEAD
build_svt ref

# build encoders
for i in *.patch; do
    [ -f "$i" ] || break
    build=$i
    git -C svt apply < $i
    build_svt "${i::-6}"
done
