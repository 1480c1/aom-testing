import json
import boto3
import datetime 

sns = boto3.client('sns')
cost_explorer = boto3.client('ce')

'''ARN to send emails to'''
arn = ''

def lambda_handler(event, context):
    two_weeks_ago, today = go_two_weeks_back()

    two_week_cost = get_two_weeks_cost(two_weeks_ago, today)
    
    send_email(two_weeks_ago, today, two_week_cost)


def get_two_weeks_cost(two_weeks_ago, today):
    cost_forecast = cost_explorer.get_cost_and_usage(
    TimePeriod={
    'Start': two_weeks_ago,
    'End': today
        },
    Metrics=['BLENDED_COST'],
    Granularity='DAILY')
    
    two_week_cost = 0
    
    for cost_info in cost_forecast['ResultsByTime']:
        cost = cost_info['Total']['BlendedCost']['Amount']
        two_week_cost += float(cost)

    return two_week_cost
    
    
def go_two_weeks_back():
    today = datetime.date.today()
    two_weeks = datetime.timedelta(days = 14)
    two_weeks_ago = today - two_weeks
    
    return str(two_weeks_ago), str(today)
    
    
def send_email(two_weeks_ago, today,two_week_cost):
    subject = 'AWS Costs for {} - {}'.format(two_weeks_ago,today)

    message = '''Hi, \n The blended costs for the past two weeks was {} USD'''.format(two_week_cost)
    
    response = sns.publish(
        TopicArn=arn,
        Subject=subject,
        Message=message
    )
    return response
    