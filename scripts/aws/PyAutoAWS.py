import os
import shutil
import subprocess
import paramiko
import socks
import datetime 
import time
import glob
import re
import boto3
import sys
import math
import csv
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.message import Message
import sys
'''Instance Types: | c5.xlarge -> Cheap testing machine | c5.24xlarge -> SPIE2021 machine |'''

'''-------------------------------------------------------------------Test Settings-----------------------------------------------------------------------------------------'''

test_sets = ['SPIE2020_ffmpeg_svt']#['UGC_SPIE2021_svt', 'UGC_SPIE2021_svt_fastdecode'] 
version = 'latest'  # latest checks out latest tag, otherwise put desired tag or commit

TAG_MODE = True    # If True, checks for new tag everyday and executes tests when new tag is detected
TAG_CLIPSET = 'atem' # ugc/elfluente
TAG_ENCODER = 'svt' # svt/aom 
EMAIL_RECIPIENTS = []



'''-------------------------------------------------------------------Test Configurations-------------------------------------------------------------------------------'''


test_repo = {
'UGC_SPIE2021_svt' : {'presets': [0,1,2,3,4,5,6,7,8,9,10,11,12,13],
                    'test_config' : 'SPIE2021_svt',
                    'clipset' : 'UGC',
                    'intraperiod' : '-1',
                    'added_params' : '',
                    'instance_type' : "c5.24xlarge",
                    'image_tag' : 'svt_prod'},

'UGC_SPIE2021_svt_fastdecode' : {'presets': [0,1,2,3,4,5,6,7,8,9,10,11,12,13],
                    'test_config' : 'SPIE2021_svt',
                    'clipset' : 'UGC',
                    'intraperiod' : '-1',
                    'added_params' : '--fast-decode 1',
                    'instance_type' : "c5.24xlarge",
                    'image_tag' : 'svt_prod'},
                    
'Elfuente_SPIE2021_svt' : {'presets': [0,1,2,3,4,5,6,7,8,9,10,11,12,13],
                    'test_config' : 'SPIE2021_svt',
                    'clipset' : 'elfluente',
                    'intraperiod' : '-1',
                    'added_params' : '',
                    'instance_type' : "c5.24xlarge",
                    'image_tag' : 'svt_prod'},
                    
'Elfuente_SPIE2021_svt_fastdecode' : {'presets': [0,1,2,3,4,5,6,7,8,9,10,11,12,13],
                    'test_config' : 'SPIE2021_svt',
                    'clipset' : 'elfluente',
                    'intraperiod' : '-1',
                    'added_params' : '--fast-decode 1',
                    'instance_type' : "c5.24xlarge",
                    'image_tag' : 'svt_prod'},   
                    
'Elfuente_SPIE2021_aom' : {'presets': [0,1,2,3,4,5,6],
                    'test_config' : 'SPIE2021_aom',
                    'clipset' : 'elfluente',
                    'intraperiod' : '-1',
                    'added_params' : '',
                    'instance_type' : "c5.24xlarge",
                    'image_tag' : 'svt_prod'},  
                    
'UGC_SPIE2021_aom' : {'presets': [0,1,2,3,4,5,6],
                    'test_config' : 'SPIE2021_aom',
                    'clipset' : 'UGC',
                    'intraperiod' : '-1',
                    'added_params' : '',
                    'instance_type' : "c5.24xlarge",
                    'image_tag' : 'svt_prod'}, 
                    
'SPIE2020_ffmpeg_svt' : {'presets': [0,1,2,3,4,5,6,7,8,9,10,11,12,13],
                    'test_config' : 'SPIE2020_ffmpeg_svt',
                    'clipset' : 'ffmpeg_clips',
                    'intraperiod' : '120',
                    'added_params' : '',
                    'instance_type' : "c5.24xlarge",
                    'image_tag' : 'svt_prod'},                    
                    
'svt_testing' : {'presets': [13],
                    'test_config' : 'SPIE2020_ffmpeg_svt',
                    'clipset' : 'obj-1',
                    'intraperiod' : '-1',
                    'added_params' : '',
                    'instance_type' : "c5.24xlarge",
                    'image_tag' : 'svt_prod'},
                    
'svt_testing_fast_decode' : {'presets': [13],
                    'test_config' : 'SPIE2021_svt_for_testing',
                    'clipset' : 'obj-1',
                    'intraperiod' : '-1',
                    'added_params' : '--fast-decode 1',
                    'instance_type' : "c5.24xlarge",
                    'image_tag' : 'svt_prod'},                    
}

'''-------------------------------------------------------------------MAIN-------------------------------------------------------------------------------'''


def main():
    while TAG_MODE:
        latest_tag = check_tag()
        if latest_tag:
            print("New tag has been found!")
            break
        print("No new tag yet")
        time.sleep(600) # Checks for new tag everyday
        
    if 'rc' in latest_tag:
        print('Not Running Release Candiate revisions')
        sys.exit()
    
    if latest_tag.split('.')[-1] != '0':
        preset_groups = list()
        for test_set in test_sets:
            preset_group = list()
            for preset in test_repo[test_set]['presets']:
                if preset >= 7:
                    preset_group.append(preset)
                    print('preset',preset)
            preset_groups.append(preset_group)
    else:
        preset_groups = [test_repo[test_set]['presets'] for test_set in test_sets]

    '''Test Execution'''
    print('splitting preset')
    split_preset_groups = get_split_presets(preset_groups)
    print('generating tf')
    generate_main_tf(test_sets,split_preset_groups)
    print('generating user data')
    generate_user_data(test_sets)
    print('running tf')
    run_main_tf()

    '''Test Collection'''
    print('getting results')

    get_results()
    
    results = get_comparisons()
    
    send_results(results)
    
'''-------------------------------------------------------------------Test Execution-------------------------------------------------------------------------------'''

def check_tag():
    global version, test_sets
    if not os.path.isfile("tags.txt"):
        with open("tags.txt",'w'): pass
    with open("tags.txt") as tag_file:
        previous_tag = tag_file.readlines()

        if TAG_ENCODER == 'svt':
            command = "git -c versionsort.suffix=- ls-remote  --refs --sort v:refname --tags https://gitlab.com/AOMediaCodec/SVT-AV1.git"
            if TAG_CLIPSET.lower() == 'ugc':
                test_sets = ['UGC_SPIE2021_svt', 'UGC_SPIE2021_svt_fastdecode']
            elif TAG_CLIPSET.lower() == 'elfluente':
                test_sets = ['Elfuente_SPIE2021_svt', 'Elfuente_SPIE2021_svt_fastdecode']
            elif TAG_CLIPSET.lower() == 'atem':
                test_sets = ['SPIE2020_ffmpeg_svt']
            else:
                print('[ERROR!] Invalid clipset for TAG CHECK detected. Quitting...')
                sys.exit()
        elif TAG_ENCODER == 'aom':
            command = "git -c versionsort.suffix=- ls-remote  --refs --sort v:refname --tags https://aomedia.googlesource.com/aom.git"
            test_sets = ['UGC_SPIE2021_aom', 'Elfuente_SPIE2021_aom']
        else:
            print('[ERROR!] In valid Tag Encoder Selection. Quitting...')
            sys.exit()
            
        current = subprocess.check_output(command, shell=True)
        current_str = current.decode()
        tags = current_str.strip('\n').split('\n')
        latest_tag = tags[-1].split('/')[-1]
        latest_revision = tags[-1].split('\t')[0]

        if latest_tag in previous_tag:
            print('latest tag {} already run'.format(latest_tag))
            return False

        print('running latest tag {}'.format(latest_tag))
        version = latest_tag

    with open("tags.txt",'a') as tag_file:
        tag_file.write('\n')
        tag_file.write(latest_tag)

        
    return latest_tag


def get_split_presets(preset_groups):
    split_preset_groups = list()

    for test_set, preset_group in zip(test_sets,preset_groups):
        split_presets = list()
        temp_presets = list()
        index = 0
        for count, preset in enumerate(preset_group):
            temp_presets.append(str(preset))
            if count >= index * 2 or count == len(preset_group)-1:
                split_presets.append(temp_presets)
                count += 1
                index = count
                temp_presets = list()
        split_preset_groups.append(split_presets)
    print('split_preset_groups',split_preset_groups)
    return split_preset_groups
    
    
def generate_main_tf(test_set,split_presets):
    value_list = list()
    
    with open('main.tf','w') as out:
        out.write(initialise_string % (access_key, secret_key))
        for test,split_preset_group in zip(test_sets,split_presets):
            ami = get_latest_image(test)
            clipset = test_repo[test]['clipset']
            instance_type = test_repo[test]['instance_type']
            test_config = test_repo[test]['test_config']
            intraperiod = test_repo[test]['intraperiod']
            added_params = test_repo[test]['added_params']
            added_param_name = ''
            if added_params:
                added_param_name = added_params.split("--")[1].split(" ")[0]
            for preset_group in split_preset_group:
                print(preset_group)
                
                presets = ','.join(preset_group)
                if len(presets) > 2:
                    temp_preset = presets.split(",")
                    machine_name = '{}_M{}-{}'.format(test, temp_preset[0], temp_preset[-1])
                else:
                    machine_name = '{}_M{}'.format(test, presets)
                
                value_list.append('aws_instance.%s.public_dns' % machine_name)
                
                machine_string = machine_template % (machine_name,ami,instance_type,test_config,presets,clipset,intraperiod,added_params,added_param_name,machine_name)
                out.write(machine_string)
                
        end_string = end_string_template % (str(value_list).replace("'",''))
        out.write(end_string)

        
def get_latest_image(test):
    dates = list()
    filter = [{'Name':'tag:{}'.format(test_repo[test]['image_tag']), 'Values':['']}]
    images = ec2.describe_images(Owners=['self'], Filters=filter)
    date_map = dict()
    
    for image in images['Images']:
        date_split = image['CreationDate'].replace('T','-').replace(':','-').split('-')
        date = datetime.datetime(int(date_split[0]),int(date_split[1]),int(date_split[2]),int(date_split[3]),int(date_split[4]))
        dates.append(date)
        date_map[date] = image['ImageId']

    latest_image = date_map[max(dates)]
    return latest_image

        
def generate_user_data(test_sets):
    if any('svt' in test for test in test_sets):
        encoder = 'svt'
        long_enc = 'SvtAv1EncApp'
    elif any('aom' in test for test in test_sets):
        encoder = 'aom'
        long_enc = 'aomenc'
    if any('ffmpeg' in test for test in test_sets):
        encoder = 'ffmpeg'
        long_enc = 'ffmpeg'    
    user_data = user_data_template % (encoder, version, long_enc)

    with open('setup.sh','w') as setup:
        setup.write(user_data)
        
        
def run_main_tf():
    terraform_commands = ['%s init -upgrade'%terraform_bin,
                          '%s apply -auto-approve'%terraform_bin,
                          '%s output -json instances > dns.txt' % terraform_bin]
    
    for command in terraform_commands:
        pipe = subprocess.Popen(command, shell = True)
        pipe.wait()


'''-------------------------------------------------------------------Result Retrieval-------------------------------------------------------------------------------'''


def get_results():
    completed_tests = []
    machine_ips = get_machine_ips()
    ec2info = get_instance_ids()
    while 1:
        for machine_ip in machine_ips:
            if machine_ip not in completed_tests and get_results_helper(machine_ip):
                stop_finished_machine(ec2info[machine_ip])
                completed_tests.append(machine_ip)
        if len(completed_tests) != len(machine_ips):
            time.sleep(3600)
        else:
            break
        
    print("Downloaded results")

    
def get_instance_ids():
    running_instances = ec2_resource.instances.filter(Filters=[{
        'Name': 'instance-state-name',
        'Values': ['running']}])

    ec2info = dict()
    for instance in running_instances:    
        ec2info[instance.public_dns_name] = instance.id

    return ec2info


def stop_finished_machine(instance_id):
    
    response = ec2.stop_instances(
    InstanceIds=[instance_id],
    Force=True
    )
    
    print('stop response',response)


def get_machine_ips():
    with open('dns.txt','r') as f:
        dns = re.findall(r'"(.*?)"',f.read())
        return dns


def get_results_helper(ip):
    print('ip',ip)
    ssh = None
    result_dir = RESULTS
    while not ssh:
        ssh = get_ssh_connection(ip, 'ubuntu', '', True)
        if not ssh:
            time.sleep(10)
    
    sftp = ssh.open_sftp()
    done = False
    remote_dir = '/home/ubuntu/'

    if not os.path.isdir(result_dir):
        os.mkdir(result_dir)
    
    for filename in sftp.listdir(remote_dir):
        try:
            if filename.endswith('.tar'):
                print('{}{}'.format(remote_dir, filename))
                sftp.get('{}{}'.format(remote_dir, filename), os.path.join(result_dir, filename))
                done = True
        except Exception as e:
            print(e)
            pass
        
        try:
            for file in sftp.listdir(filename):
                if "result" in file:
                    print('Downloading {}/{}/{} to {}\\{}'.format(remote_dir, filename, file, RESULTS,file))
                    sftp.get('{}/{}/{}'.format(remote_dir, filename, file), os.path.join(result_dir, file))
        except Exception as e:
            print(e)
            pass
            
    sftp.close()
    
    if not done:
        print("Not ready yet")
        return False
    else:
        print("Downloaded files")
        return True


def text_frame(string_lst, width=55):
    g_line = "+{0}+".format("-"*(width-2))

    print(g_line)

    for line in string_lst:
        print("| {0:<{1}} |".format(line, width-4))

    print(g_line)


def get_ssh_connection(ssh_machine, ssh_username, ssh_password, proxy):
    text_frame('Connecting to server: {}'.format(ssh_machine).splitlines())
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    attempts = 0
    success = False
    
    while attempts <=3:
        try:
            if proxy:
                command=socks.socksocket()
                command.set_proxy(
                    proxy_type=socks.SOCKS5,
                    addr="proxy-us.intel.com",
                    port=1080)
                command.connect((ssh_machine,22))
                
                client.connect(
                    hostname=ssh_machine,
                    username=ssh_username,
                    port=22,
                    sock=command,
                    key_filename=PEM_FILE,
                    timeout=10)
                success = True
            else:
                client.connect(
                    hostname=ssh_machine,
                    username=ssh_username,
                    port=22,
                    password=ssh_password,
                    timeout=10)

                success = True
                
        except Exception as e:
            print('connection failed, retrying attempt %s | %s' % (attempts, e))
            attempts += 1

        if success:
            return client
            break
        
    print('Connection Failed')
    return None


'''-------------------------------------------------------------------Result Evaluation-------------------------------------------------------------------------------'''


def send_results(results):
    accumulated_html_template = ''
    email_template_folder = os.path.join(os.getcwd(),'email_template')
    email_template = os.path.join(email_template_folder,'result.html')
    '''sort results'''
    for index in range(len(results)):
        results[index][0] = re.sub(r'_\d+-\d+-\d+','',results[index][0])
        results[index][1] = re.sub(r'_\d+-\d+-\d+','',results[index][1])
        
    sorted_results = sorted([x for x in results], key=lambda x: ('_'.join(x[1].split('_')[:-1]),'_'.join(x[0].split('_')[:-1]), int(x[0].split('_')[-1][1:])))
    for result in sorted_results:
        mod_name = result[0]
        ref_name = result[1]
        psnr_y = "{:.4%}".format(result[2]) 
        ssim_y = "{:.4%}".format(result[3]) 
        vmaf_y = "{:.4%}".format(result[4]) 
        vmaf_neg_y = "{:.4%}".format(result[5])  
        avg_bdr = "{:.4%}".format(result[6]) 
        mod_cycles = result[7] 
        ref_cycles =  result[8]
        mod_decode = result[9]
        ref_decode = result[10]
        enc_speed_dev = "{:.4%}".format(result[11]) 
        dec_speed_dev = "{:.4%}".format(result[12]) 
        mem_dev = "{:.4%}".format(result[13]) 
        abs_mem_dev = "{:.4%}".format(result[14]) 
        accumulated_html_template += html_template.format(**vars())
        accumulated_html_template += '\n'

    with open(email_template) as html:
        html_content = html.read()
        html = html_content.replace('''Clip Memory Deviation ALL</td>
 </tr>''',accumulated_html_template)
        html = html.replace(''' </tr>

    Clip Memory Deviation ALL</td>
 </tr>''','</tr>')
    with open('html_bug.html','w') as  bug:
        bug.write(html)
        
    send_email(html)


def send_email(html):#(title, recipients, sender, html):
    print('we have reached the mailing room')    

    message = MIMEMultipart("alternative", None, [MIMEText(html,'html')])
    sender = 'Auto_Tag_sender@intel.com'
    message['From'] = 'Auto_Tag_sender@intel.com'
    message['To'] = ", ".join(EMAIL_RECIPIENTS)
    message['Subject'] = '{} {} {} Tag Report'.format(test_repo[test_sets[0]]['clipset'],TAG_ENCODER,version)
    
    print('Sending Results...')
    s = smtplib.SMTP('smtp.intel.com',25)
    s.sendmail(sender, EMAIL_RECIPIENTS, message.as_string())
    s.quit()
    print('Results Sent!')


'''-------------------------------------------------------------------Get BDR Comparison Pairs-------------------------------------------------------------------------------'''


def get_comparisons():
    global BDR_HEADER, SYSTEM_HEADER, BDR_RESULTS, SYSTEM_RESULTS

    RESULTS = list()
    cvh_comparison = None
    squashed_encoder_list = list()
    
    for result_file in result_files:
        with open(result_file) as file:
            content = file.readlines()[1:]
            squashed_encoder_list.extend(sorted(list(set([x.split('\t')[1] for x in content if x.split('\t')[1]]))))

    selections = auto_comparison_selector(squashed_encoder_list)
    for selection in selections:
        for select in selection:
            for encoder_pair in select:
                mod = encoder_pair[0].split('|')[1]
                ref = encoder_pair[0].split('|')[0]
                presets = encoder_pair[1]
                sorted_presets = sorted([x for x in presets], key=lambda x: (int(x.split('M')[-1])))

                perform_comparisons(mod,ref,sorted_presets)
                
    print('BDR_RESULTS',BDR_RESULTS)
    print('SYSTEM_RESULTS',SYSTEM_RESULTS)
    
    for bdr_results, system_results in zip(BDR_RESULTS,SYSTEM_RESULTS):
        RESULTS.append(system_results[:2] + bdr_results + system_results[2:])
        
    print('\n\n\n\nRESULTS',RESULTS)
    return RESULTS

    
def auto_comparison_selector(squashed_encoder_list):
    ref_selection = None
    grouped_encoders = get_grouped_encoders(squashed_encoder_list)            

    '''Get paired encoder selections'''
    for index,grouped_encoder in enumerate(grouped_encoders):
        if ('av1' in grouped_encoder.lower() and 'svt' not in grouped_encoder.lower()) or ('libaom' in grouped_encoder.lower()):
            ref_selection = '{}_{}'.format(grouped_encoder,grouped_encoders[grouped_encoder][0])

    if ref_selection:
        paired_encoders_1 = get_paired_encoders(squashed_encoder_list,grouped_encoders,ref_selection,'1')
        keys_to_delete = [x for x in paired_encoders_1 if (x.split('|')[1] in x.split('|')[0])]

        for key in keys_to_delete:
            paired_encoders_1.pop(key)

        pair_selections_1 = [str(x) for x in range(len(paired_encoders_1))]
        enc_selections_1 = get_enc_selections(paired_encoders_1,pair_selections_1)
    paired_encoders_2 = get_paired_encoders(squashed_encoder_list,grouped_encoders,None)
    keys_to_delete = [x for x in paired_encoders_2 if ('_'.join(ref_selection.split('_')[:-1]) in x) or ('decode' in x.split('|')[0])]

    for key in keys_to_delete:
        paired_encoders_2.pop(key)
    
    pair_selections_2 = [str(x) for x in range(len(paired_encoders_2))]
    enc_selections_2 = get_enc_selections(paired_encoders_2,pair_selections_2)
    
    print('Auto comparisons the following pairs\n')
    print('\nenc_selections_2',enc_selections_2)
    print('\n')
    if ref_selection:
        enc_selections = [[enc_selections_1],[enc_selections_2]]
    else:
        enc_selections = [[enc_selections_2]]
        
    return enc_selections


def get_grouped_encoders(squashed_encoder_list,comparison_selection = None):
    for index, enc_name in enumerate(squashed_encoder_list):
        if enc_name[-1].isdigit():
            enc_group = '_'.join(enc_name.split('_')[0:-1])
            enc_preset = enc_name.split('_')[-1]

            if enc_group not in grouped_encoders:
                grouped_encoders[enc_group] = [enc_preset]
            else:
                grouped_encoders[enc_group].append(enc_preset)

            if comparison_selection == '1':
                print('({0}) -> {1}'.format(index, enc_name))
    return grouped_encoders


def get_paired_encoders(squashed_encoder_list,grouped_encoders,ref_selection=None,comparison_selection =None):
    paired_encoders = dict()
    if comparison_selection == '1':
        ref_enc = ref_selection

        for mod_enc in grouped_encoders:
            if mod_enc != ref_enc:
                mod_presets = grouped_encoders[mod_enc]
                pair_id = '{0}|{1}'.format(ref_enc, mod_enc)
                paired_encoders[pair_id] = sorted(mod_presets, key=lambda x: int(x.replace('M', '')))
    else:
        for ref_enc in grouped_encoders:
            for mod_enc in grouped_encoders:
                if mod_enc != ref_enc:
                    ref_presets = grouped_encoders[ref_enc]
                    mod_presets = grouped_encoders[mod_enc]
                    common_presets = [preset for preset in ref_presets if preset in mod_presets]
                    
                    if common_presets:
                        pair_id = '|'.join('{0}|{1}'.format(ref_enc, mod_enc).split('|'))
                        paired_encoders[pair_id] = common_presets
                        pair_id = '|'.join('{1}|{0}'.format(ref_enc, mod_enc).split('|'))
                        paired_encoders[pair_id] = common_presets
    return paired_encoders


def get_enc_selections(paired_encoders,pair_selections):
    if not paired_encoders:
        return []
    while True:
        valid = False
        enc_selections = list()

        pairs = list(paired_encoders.items())

        '''Final pair selection'''

        for pair_selection in pair_selections:

            if pair_selection.strip().isdigit() == False or int(pair_selection) >= len(pairs) or int(pair_selection) < 0:
                print('Invalid Selection, Retry?')
                valid = False
                break
            else:
                enc_selections.append(pairs[int(pair_selection.strip())])
                print(enc_selections)
                valid = True
        if valid:
            break
    return enc_selections

                    
'''-------------------------------------------------------------------Perform BDR Comparisons-------------------------------------------------------------------------------'''


def perform_comparisons(mod,ref,presets):


    '''puts data from result txt into a large dictionary'''
    bdr_data, system_data = process_data(result_files)

    '''generates a dictionary of bdrate values'''
    detailed_bdr_results = get_detailed_bdr_results(mod, ref, presets, bdr_data)
    
    '''generates a dictionary of system speed, memory values'''
    system_results = get_system_results(mod, ref, presets, system_data)

    '''# generates overall summary of metrics by preset, per metric'''
    averaged_bdr_results = get_averaged_bdr_results(detailed_bdr_results, presets) 
    
##    write_results(processed, presets) # generates text file with metrics per clip, per preset
    BDR_RESULTS.extend(averaged_bdr_results)
    SYSTEM_RESULTS.extend(system_results)
    #return averaged_bdr_results, system_results


def process_data(files):
    '''Loop through all files in results folder'''
    bdr_data = dict()
    system_data = dict()
    for file in files:
        '''Split the results between cvh and non-cvh'''
        if "convex" not in file:
            '''Loop through all the system metrics. Speed, mem etc...'''
            for sys_metric in SYS_METRICS:
                metric_name = sys_metric
                process_data_helper(file, metric_name, system_data)
        else:
            '''If CVH file, target the metric specified in the file name'''
            metric_name = os.path.split(file)[-1].split("_c")[0]
            process_data_helper(file, metric_name, bdr_data)


    return bdr_data, system_data

def process_data_helper(file, metric_name, data):
    last_line_number = len(open(file).readlines()) - 2

    '''Open result File'''
    with open(file, mode='r') as csv_file:
        content = csv.DictReader(csv_file, delimiter='\t')
        prev_sequence = None
        rates = list()
        metrics = list()
        preset_pattern = re.compile(r'M\d\d?')
        index = 0
        
        for row in content:
            row['INPUT_SEQUENCE'] = '_'.join(row['INPUT_SEQUENCE'].split('_')[:-1])
            '''Populate the MegaDict entries accordingly after each shift in sequence naming'''                
            if (row['INPUT_SEQUENCE'] != prev_sequence and prev_sequence) or (index == last_line_number):
                '''Initialize the third layer of MegaDict'''
                if prev_sequence not in data[encoder_name][metric_name][preset]:
                    if metric_name in SYS_METRICS:
                        data[encoder_name][metric_name][preset][prev_sequence] = [metrics]
                    else:
                        data[encoder_name][metric_name][preset][prev_sequence] = [rates,metrics]
                else:
                    if metric_name in SYS_METRICS:
                        '''System Results'''
                        data[encoder_name][metric_name][preset][prev_sequence].append(metrics)
                    else:
                        '''BDR Results'''

                        data[encoder_name][metric_name][preset][prev_sequence].append(rates)
                        data[encoder_name][metric_name][preset][prev_sequence].append(metrics)
                rates = list()
                metrics = list()
                
            '''Skip over empty lines'''
            if not row['ENC_NAME']:
                index += 1
                continue

            '''Get Enc Name'''
            if row['ENC_NAME']:
                encoder_name = re.search(r'(.+?)(?=_M)', row['ENC_NAME']).group()

            '''Format input Sequence'''
            row['INPUT_SEQUENCE'] = row['INPUT_SEQUENCE'].replace('_lanc','')
            resolution_search = re.search(r'(\d+x\d+)to(\d+x\d+)',row['INPUT_SEQUENCE'])
            if resolution_search:
                row['INPUT_SEQUENCE'] = re.sub(r'\d+x\d+to\d+x\d+',resolution_search.group(2),row['INPUT_SEQUENCE'])
            preset = re.search(preset_pattern, row['ENC_NAME'])[0]

            '''Initialize the first layer of MegaDict'''
            if encoder_name not in data:
                data[encoder_name] = dict()

            '''Initialize the second layer of MegaDict'''
            if metric_name not in data[encoder_name]:
                data[encoder_name][metric_name] = dict()
                
            '''Initialize the Third layer of MegaDict'''
            if preset not in data[encoder_name][metric_name]:
                data[encoder_name][metric_name][preset] = dict()

            index += 1 
            prev_sequence = row['INPUT_SEQUENCE']
            
            '''Accumulate data'''
            if metric_name in SYS_METRICS:
                if row[metric_name].strip() and row[metric_name] != 'n/a':
                    metric = float(row[metric_name])
                    metrics.append(metric)
                else:
                    metrics.append(0)
            else:
                try:
                    if row['FILE_SIZE'].strip() and row[metric_name].strip() and row['FILE_SIZE'] != 'n/a' and row[metric_name] != 'n/a':
                        clips.add(prev_sequence)
                        metric = float(row[metric_name])
                        if metric == 0:
                            continue
                        
                        rate = float(row['FILE_SIZE'])
                        rates.append(rate)
                        metrics.append(metric)
                except:
                    print('row',row)

    return data


def get_detailed_bdr_results(mod, ref, presets, bdr_data):
    bdr_results = dict()
    preset_search = re.search(r'_(M\d+)', ref)
 

    all_clips = sorted(list(clips))
    
    for metric in METRICS:
        bdr_results[metric] = dict()
        for preset in presets:
            if preset_search: 
                ref_preset = preset_search[1]
                ref = ref.split("_M")[0]
            else:
                ref_preset = preset
                
            bdr_results[metric][preset] = dict()
            for clip in all_clips:
                bdrate = bdRateExtend(bdr_data[ref][metric][ref_preset][clip][0],
                                      bdr_data[ref][metric][ref_preset][clip][1], 
                                      bdr_data[mod][metric][preset][clip][0], 
                                      bdr_data[mod][metric][preset][clip][1], 'None', False)
                
                bdr_results[metric][preset][clip] = bdrate

    return bdr_results


def get_system_results(mod_name, ref_name, presets,system_data):
    ref_preset = None
    preset_search = re.search(r'_(M\d+)', ref_name)

    system_results = list()
    
    for preset in presets:
        if preset_search: 
            ref_preset = preset_search[1]
            ref_name = ref_name.split("_M")[0]
        else:
            ref_preset = preset

        mod = system_data[mod_name]
        ref = system_data[ref_name]
        
        result_mod_name = '{}_{}'.format(mod_name,preset)
        result_ref_name = '{}_{}'.format(ref_name,ref_preset)
    
        total_mod_encode_cycles = 0
        total_ref_encode_cycles = 0
        total_mod_decode_cycles = 0
        total_ref_decode_cycles = 0
        
        max_mem_mod = 0
        max_mem_ref = 0
        absolute_max_memory_deviation = 0
          
        for clip in mod['ENCODE_USER_TIME'][preset].keys():
            enc_ref_cycles = sum(ref['ENCODE_USER_TIME'][ref_preset][clip][0]) + sum(ref['ENCODE_SYS_TIME'][ref_preset][clip][0])
            dec_ref_cycles = sum(ref['DECODE_USER_TIME'][ref_preset][clip][0]) + sum(ref['DECODE_SYS_TIME'][ref_preset][clip][0])

            enc_mod_cycles = sum(mod['ENCODE_USER_TIME'][preset][clip][0])  + sum(mod['ENCODE_SYS_TIME'][preset][clip][0])
            dec_mod_cycles = sum(mod['DECODE_USER_TIME'][preset][clip][0])  + sum(mod['DECODE_SYS_TIME'][preset][clip][0])
            
            if enc_mod_cycles:
                enc_speed_dev = ((enc_ref_cycles/enc_mod_cycles)-1)
            else:
                enc_speed_dev = "not collected"
                
            if enc_mod_cycles:
                dec_speed_dev = ((dec_ref_cycles/dec_mod_cycles)-1)
            else:
                dec_speed_dev = "not collected"

            total_mod_encode_cycles += enc_mod_cycles
            total_ref_encode_cycles += enc_ref_cycles
            total_mod_decode_cycles += dec_mod_cycles
            total_ref_decode_cycles += dec_ref_cycles
            
            memory_dev = max(mod['MAX_MEMORY'][preset][clip][0])/max(ref['MAX_MEMORY'][ref_preset][clip][0]) -1
                
            max_mem_mod = max(max_mem_mod, max(mod['MAX_MEMORY'][preset][clip][0]))
            max_mem_ref = max (max_mem_ref, max(ref['MAX_MEMORY'][ref_preset][clip][0]))

            if abs(memory_dev) > abs(absolute_max_memory_deviation):
                absolute_max_memory_deviation = memory_dev
                
        overall_encode_speed_deviation = (total_ref_encode_cycles/total_mod_encode_cycles)-1
        overall_decode_speed_deviation = (total_ref_decode_cycles/total_mod_decode_cycles)-1
        max_mem_dev = max_mem_mod/max_mem_ref - 1

        system_results.append([result_mod_name,
                       result_ref_name,
                       total_mod_encode_cycles/1000,
                       total_ref_encode_cycles/1000,
                       total_mod_decode_cycles/1000,
                       total_ref_decode_cycles/1000,
                       overall_encode_speed_deviation,
                       overall_decode_speed_deviation,
                       max_mem_dev,
                       absolute_max_memory_deviation])
        
    return system_results


def get_averaged_bdr_results(detailed_bdr_results,presets):
    output = open('averaged_bdr_results.txt', 'a+')

    all_clips = sorted(list(clips))
    
    averaged_bdr_results = list()
    
    for preset in presets:
        results = list()
        line = '{}: '.format(preset)
        average_metrics = 0
        
        for metric in METRICS:
            metric_value = 0

            for clip in all_clips:
                metric_value += detailed_bdr_results[metric][preset][clip]

            metric_value = metric_value/len(all_clips)
            line = line + '{}: {:.4%}\t'.format(metric, metric_value)

            if metric != 'VMAF_NEG':
                average_metrics += metric_value

            results.append(metric_value)
            
        average_metrics = average_metrics/3
        line = line + 'average PSNR/SSIM/VMAF: {:.4%}'.format(average_metrics)
        output.write(line+"\n\n")
        results.append(average_metrics)
        
        averaged_bdr_results.append(results)
        
    return averaged_bdr_results

'''-------------------------------------------------------------------CVH Macro-------------------------------------------------------------------------------'''


def bdRateExtend(rateA, distA, rateB, distB, bMode, bRange):
    # type: (list[float], list[float], list[float], list[float], str, bool) -> float
    minPSNRA = float(min(distA))
    maxPSNRA = float(max(distA))
    minPSNRB = float(min(distB))
    maxPSNRB = float(max(distB))

    minMinPSNR = min(minPSNRA, minPSNRB)
    maxMinPSNR = max(minPSNRA, minPSNRB)
    minMaxPSNR = min(maxPSNRA, maxPSNRB)
    maxMaxPSNR = max(maxPSNRA, maxPSNRB)

    minPSNR = minMinPSNR
    maxPSNR = minMaxPSNR

    if bRange:
        if (minPSNRA > maxPSNRB or minPSNRB > maxPSNRA):
            bdRate = 0
        else:
            bdRate = (maxPSNR - maxMinPSNR) / (maxPSNRA - minPSNRA)
            
        if (maxPSNRB < maxPSNRA):
            bdRate = -1 * bdRate

        return bdRate

    if (bMode == "LowAlways"):
        minPSNR = minMinPSNR
        maxPSNR = minMaxPSNR
    elif (bMode == "HighAlways"):
        minPSNR = maxMinPSNR
        maxPSNR = maxMaxPSNR
    elif (bMode == "BothAlways"):
        minPSNR = minMinPSNR
        maxPSNR = maxMaxPSNR
    elif (bMode == "None" or not (minPSNRA > maxPSNRB or minPSNRB > maxPSNRA)):
        if (minPSNRA > maxPSNRB):
            return 1
        if (minPSNRB > maxPSNRA):
            return -1
        minPSNR = maxMinPSNR
        maxPSNR = minMaxPSNR
    elif (bMode == "Low"):
        minPSNR = minMinPSNR
        maxPSNR = minMaxPSNR
    elif (bMode == "High"):
        minPSNR = maxMinPSNR
        maxPSNR = maxMaxPSNR
    elif (bMode == "Both"):
        minPSNR = minMinPSNR
        maxPSNR = maxMaxPSNR

    vA = bdRIntEnh(rateA, distA, minPSNR, maxPSNR)
    vB = bdRIntEnh(rateB, distB, minPSNR, maxPSNR)
    avg = (vB - vA) / (maxPSNR - minPSNR)

    bdRate = pow(10, avg) - 1
    return bdRate

#  Enhanced BD Rate computation method that performs extrapolation instead of
#  Computing BDRate within the "common interval, when and only when there is no PSNR overlap.


def bdRIntEnh(rate, dist, low, high, distMin=0, distMax=1E+99):
    # type: (list[float], list[float], float, float, int, int) -> float
    elements = len(rate)

    log_rate = [0] * (elements + 3)
    log_dist = [0] * (elements + 3)

    rate, dist, log_rate, log_dist, elements = addValues(
        rate, dist, log_rate, log_dist, elements)

    # Remove duplicates and sort data
    log_rate = sorted(set(log_rate))
    log_dist = sorted(set(log_dist))

    log_rate.append(0)
    log_rate.append(0)
    log_dist.append(0)
    log_dist.append(0)

    # If plots do not overlap, extend range
    # Extrapolate towards the minimum if needed
    if (log_dist[1] > low):
        for i in range(1, elements):
            log_rate[elements + 2 - i] = log_rate[elements + 1 - i]
            log_dist[elements + 2 - i] = log_dist[elements + 1 - i]

        elements = elements + 1
        log_dist[1] = low
        log_rate[1] = log_rate[2] + (low - log_dist[2]) * \
            (log_rate[2] - log_rate[3]) / (log_dist[2] - log_dist[3])

    # Extrapolate towards the maximum if needed
    if (log_dist[elements] < high):
        log_dist[elements + 1] = high
        log_rate[elements + 1] = log_rate[elements] + (high - log_dist[elements]) * (
            log_rate[elements] - log_rate[elements - 1]) / (log_dist[elements] - log_dist[elements - 1])
        elements = elements + 1

    result = intCurve(log_dist, log_rate, low, high,
                         elements, distMin, distMax)

    return result


def addValues(rate, dist, log_rate, log_dist, elements):
    # type: (list[float], list[float], list[float], list[float], int) -> tuple[list[float], list[float], list[float], list[float], int]
    i = 1
    for j in range(elements, 0, -1):
        # Add elements only if they are not empty
        if (len(rate) != 0 and len(dist) != 0 and rate[j-1] > 0 and dist[j-1] > 0):
            log_rate[i] = math.log(rate[j-1], 10)
            log_dist[i] = dist[j-1]
            i += 1
    elements = i - 1

    return rate, dist, log_rate, log_dist, elements


def pchipend(h1, h2, del1, del2):
    # type: (float, float, float, float) -> float
    D = ((2 * h1 + h2) * del1 - h1 * del2) / (h1 + h2)
    if (D * del1 < 0):
        D = 0
    elif ((del1 * del2 < 0) and (abs(D) > abs(3 * del1))):
        D = 3 * del1

    return D


def intCurve(xArr, yArr, low, high, elements,  distMin=0, distMax=1E+99):
    # type: (list[float], list[float], float, float, int, int, int) -> float

    H = [0] * (elements + 3)
    delta = [0] * (elements + 3)

    for i in range(1, elements):
        H[i] = xArr[i + 1] - xArr[i]
        delta[i] = (yArr[i + 1] - yArr[i]) / H[i]

    D = [0] * (elements + 3)

    D[1] = pchipend(H[1], H[2], delta[1], delta[2])

    for i in range(2, elements):
        D[i] = (3 * H[i - 1] + 3 * H[i]) / ((2 * H[i] + H[i - 1]) /
                                            delta[i - 1] + (H[i] + 2 * H[i - 1]) / delta[i])

    D[elements] = pchipend(H[elements - 1], H[elements - 2],
                           delta[elements - 1], delta[elements - 2])
    C = [0] * (elements + 3)
    B = [0] * (elements + 3)

    for i in range(1, elements):
        C[i] = (3 * delta[i] - 2 * D[i] - D[i + 1]) / H[i]
        B[i] = (D[i] - 2 * delta[i] + D[i + 1]) / (H[i] * H[i])

    result = 0
    #    Compute rate for the extrapolated region if needed
    s0 = xArr[1]
    s1 = xArr[2]

    for i in range(1, elements):
        s0 = xArr[i]
        s1 = xArr[i + 1]

        #  clip s0 to valid range
        s0 = max(s0, low, distMin)
        s0 = min(s0, high, distMax)

        #  clip s1 to valid range
        s1 = max(s1, low, distMin)
        s1 = min(s1, high, distMax)

        s0 = s0 - xArr[i]
        s1 = s1 - xArr[i]

        if (s1 > s0):
            result = result + (s1 - s0) * yArr[i]
            result = result + (s1 * s1 - s0 * s0) * D[i] / 2
            result = result + (s1 * s1 * s1 - s0 * s0 * s0) * C[i] / 3
            result = result + (s1 * s1 * s1 * s1 - s0 *
                               s0 * s0 * s0) * B[i] / 4

    return result

'''-------------------------------------------------------------------Templates-------------------------------------------------------------------------------'''

initialise_string = '''terraform {
    required_providers {
        aws = {
        source  = "hashicorp/aws"
        version = "~> 4.16"
        }
    }

    required_version = ">= 1.2.0"
}

provider "aws" {
    access_key = "%s"
    secret_key = "%s"
    region = "us-west-2"
}
''' 

machine_template = '''
resource "aws_instance" "%s" {
    ami = "%s"
    instance_type = "%s"
    user_data = templatefile("${path.module}/setup.sh", {
        test_config = "%s",
        preset = "%s",
        clipset = "%s",
        intraperiod = "%s",
        added_params = "%s",
        added_param_name = "%s"})
    vpc_security_group_ids = [data.aws_security_group.intel.id]
    associate_public_ip_address=true
    key_name = "svt"
    tags = {
        Name = "%s"
    }
}
'''
            
end_string_template = '''
data "aws_security_group" "intel" {
  id ="sg-01e243e6a24f3ede7"
}

output "instances" {

  value       = %s
}
'''

user_data_template = '''#!/bin/bash
sudo apt install clang-11 -y
encoder="%s"
version="%s"
if [ "$encoder" == "aom" ]; then
	cd /home/ubuntu/ && git clone https://aomedia.googlesource.com/aom $encoder
elif [ "$encoder" == "ffmpeg" ]; then
    cd /home/ubuntu/ && git clone https://gitlab.com/AOMediaCodec/SVT-AV1.git $encoder
    cd /home/ubuntu/ && git clone https://git.ffmpeg.org/ffmpeg.git ffmpeg_svt
else
	cd /home/ubuntu/ && git clone https://gitlab.com/AOMediaCodec/SVT-AV1.git $encoder
fi


cd /home/ubuntu/$encoder && git config --global --add safe.directory /home/ubuntu/$encoder

if [ "$version" == "latest" ]; then
    cd /home/ubuntu/$encoder && export latest=$(git describe --tags `git rev-list --tags --max-count=1`)
    echo $latest > tag.txt
else
    cd /home/ubuntu/$encoder && export latest="$version"
fi

test_dir=/home/ubuntu/"$encoder"_${clipset}_M${preset}_"$latest"_${added_param_name}
sudo mkdir -p $test_dir

cd /home/ubuntu/$encoder && sudo git checkout $latest
if [ "$encoder" == "aom" ]; then
    mkdir -p /home/ubuntu/aom/aom_build
    cd /home/ubuntu/aom/aom_build && cmake /home/ubuntu/$encoder && make -j
    cp /home/ubuntu/aom/aom_build/aomenc $test_dir
elif [ "$encoder" == "ffmpeg" ]; then
    mkdir -p /dev/shm/temp
    export CC="clang-11" && export CXX="clang++-11" && sudo -E /home/ubuntu/$encoder/Build/linux/build.sh static prefix=/dev/shm/temp
    cd /home/ubuntu/$encoder/Build/linux/Release && sudo make install
    cd /home/ubuntu/ffmpeg_svt && sudo PKG_CONFIG_PATH=/dev/shm/temp/lib/pkgconfig ./configure --prefix=/dev/shm/temp --arch=x86_64 --pkg-config-flags="--static" --enable-{gpl,static} --enable-libsvtav1 --disable-shared --cc=clang-11 --cxx=clang++-11 && make -j
    sudo cp /home/ubuntu/ffmpeg_svt/ffmpeg $test_dir
else
    export CC="clang-11" && export CXX="clang++-11" && sudo -E /home/ubuntu/$encoder/Build/linux/build.sh static release
    sudo cp /home/ubuntu/$encoder/Bin/Release/* $test_dir
fi

sudo cp /home/ubuntu/scripts/*.py $test_dir
sudo cp -r /home/ubuntu/scripts/tools $test_dir

cd $test_dir && sudo python PyGenerateCommands.py ${test_config} ${clipset} ${preset} ${intraperiod} ${added_params} && sudo ./run-%s-all-paral.sh && rm -rf resized_clips

tar cf - $test_dir | pigz -9 -p 96 > $test_dir.tar

'''


html_template = '''    Clip Memory Deviation ALL</td>
 </tr>

<tr height=53 style='height:39.75pt'>
  <td height=53 class=xl11429190 width=374 style='height:39.75pt;border-top:
  none;width:281pt'>{mod_name}</td>
  <td class=xl11529190 width=26 style='border-top:none;border-left:none;
  width:20pt'>vs.</td>
  <td class=xl11529190 width=244 style='border-top:none;border-left:none;
  width:183pt'>{ref_name}</td>
  <td class=xl11729190 width=67 style='border-top:none;border-left:none;
  width:50pt'>{psnr_y}</td>
  <td class=xl11729190 width=75 style='border-top:none;border-left:none;
  width:56pt'>{ssim_y}</td>
  <td class=xl11729190 width=75 style='border-top:none;border-left:none;
  width:56pt'>{vmaf_y}</td>
  <td class=xl11729190 width=77 style='border-top:none;border-left:none;
  width:58pt'>{vmaf_neg_y}</td>
  <td class=xl11729190 width=77 style='border-top:none;border-left:none;
  width:58pt'>{avg_bdr}</td>
  <td class=xl11929190 width=160 style='border-top:none;border-left:none;
  width:120pt'>{mod_cycles}</td>
  <td class=xl11929190 width=61 style='border-top:none;border-left:none;
  width:46pt'>{ref_cycles}</td>
  <td class=xl11929190 width=67 style='border-top:none;border-left:none;
  width:50pt'>{mod_decode}</td>
  <td class=xl12129190 width=61 style='border-top:none;border-left:none;
  width:46pt'>{ref_decode}</td>
  <td class=xl11529190 width=75 style='border-top:none;border-left:none;
  width:56pt'>{enc_speed_dev}</td>
  <td class=xl11529190 width=71 style='border-top:none;border-left:none;
  width:53pt'>{dec_speed_dev}</td>
  <td class=xl11529190 width=71 style='border-top:none;border-left:none;
  width:53pt'>{mem_dev}</td>
  <td class=xl11629190 align=right style='border-top:none;border-left:none'>{abs_mem_dev}</td>
 </tr>
'''



'''-------------------------------------------------------------------Global Variables-------------------------------------------------------------------------------'''

# Changeable Settings Start
'''path variables for Terraform'''
DIR = os.getcwd()
RESULTS = os.path.join(DIR, 'results')
DNS_FILE = os.path.join(DIR,'dns.txt')
PEM_FILE = r'svt.pem'
key_file = 'aws_keys.csv'

if sys.platform == 'linux':
    terraform_bin = os.path.join(os.getcwd(),'terraform')
else:
    terraform_bin = os.path.join(os.getcwd(),'terraform.exe')

if not os.path.isdir(RESULTS):
    os.mkdir(RESULTS)

'''Variables for BDR Compairons'''
result_directory = os.path.join(os.getcwd(), 'results')
result_files = glob.glob('%s/*.txt' % result_directory)

BDR_RESULTS = []
SYSTEM_RESULTS = []
METRICS = ['PSNR_Y', 'SSIM_Y', 'VMAF', 'VMAF_NEG']
SYS_METRICS = ['ENCODE_USER_TIME', 'ENCODE_SYS_TIME', 'DECODE_USER_TIME', 'DECODE_SYS_TIME', 'MAX_MEMORY']

clips = set()

grouped_encoders = dict()


with open(key_file) as key:
    content = key.read()
    split_content = content.replace('\n',',').split(',')
    access_key = split_content[2]
    secret_key = split_content[3]

ec2 = boto3.client(
    'ec2',
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key,
    region_name='us-west-2',
)


ec2_resource = boto3.resource(
    'ec2',
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key,
    region_name='us-west-2',
)
    
if __name__ == '__main__':

    t = time.time()
    main()
    print(time.time() - t)

